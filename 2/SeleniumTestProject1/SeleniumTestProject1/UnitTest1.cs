﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    [TestFixture]
    public class NoteCreationTest : TestBase
    {
        [Test]
        public void TheUntitledTest()
        {
            AccountData user = new AccountData("Rocket2803@gmail.com", "password");
            NoteData note = new NoteData("Note X", "Blah-blah-blah etc.");

            OpenHomePage();
            OpenLoginPage();
            Login(user);
            CreateNote(note);
            OpenNote(note);
            DeleteNote();
        }
    }
}
