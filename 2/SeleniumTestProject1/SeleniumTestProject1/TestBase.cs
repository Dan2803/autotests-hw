﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class TestBase
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver(@"C:\Program Files (x86)\Google");
            baseURL = "https://anotepad.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        public void DeleteNote()
        {
            driver.FindElement(By.LinkText("Delete")).Click();
        }

        public void OpenNote(NoteData note)
        {
            Thread.Sleep(1000);
            driver.FindElement(By.LinkText(note.Title)).Click();
        }

        public void CreateNote(NoteData note)
        {
            driver.FindElement(By.Id("edit_title")).Clear();
            driver.FindElement(By.Id("edit_title")).SendKeys(note.Title);
            driver.FindElement(By.Id("edit_textarea")).Clear();
            driver.FindElement(By.Id("edit_textarea")).SendKeys(note.Text);
            driver.FindElement(By.CssSelector("div.col-md-6 > #btnSaveNote")).Click();
        }

        public void Login(AccountData account)
        {
            driver.FindElement(By.XPath("//div[@id='bs-example-navbar-collapse-1']/div/ul/li[5]/a/span")).Click();
            driver.FindElement(By.Id("loginEmail")).Clear();
            driver.FindElement(By.Id("loginEmail")).SendKeys(account.Username);
            driver.FindElement(By.XPath("(//input[@id='password'])[2]")).Clear();
            driver.FindElement(By.XPath("(//input[@id='password'])[2]")).SendKeys(account.Password);
            driver.FindElement(By.XPath("(//button[@id='submit'])[2]")).Click();
        }

        public void OpenLoginPage()
        {
            driver.FindElement(By.CssSelector("span")).Click();
        }

        public void OpenHomePage()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
        }
        public bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
