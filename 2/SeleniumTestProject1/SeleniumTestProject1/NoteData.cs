﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTestProject1
{
    public class NoteData
    {
        public string Title { get; set; }
        public string Text  { get; set; }

        public NoteData(string title, string text)
        {
            Title = title;
            Text = text;
        }
    }
}
