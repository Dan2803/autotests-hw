﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class AuthBase : TestBase
    {
        [SetUp]
        public void SetupWithAuth()
        {
            AccountData user = new AccountData(Settings.Login, Settings.Password);
            manager.Authorization.Login(user);
        }
    }
}
