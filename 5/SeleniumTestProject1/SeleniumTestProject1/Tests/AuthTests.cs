﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    [TestFixture]
    public class AuthTests : TestBase
    {
        [Test]
        public void LogiWithValidData()
        {
            AccountData user = new AccountData(Settings.Login, Settings.Password);
            manager.Authorization.Login(user);
            Assert.IsTrue(manager.Authorization.IsLoggedIn());
            manager.Authorization.Logout();
        }

        [Test]
        public void LogiWithInvalidData()
        {
            AccountData user = new AccountData("R2D2@gmail.com", "pass");
            manager.Authorization.Login(user);
            Assert.IsTrue(manager.Authorization.IsLoginError());
        }
    }
}
