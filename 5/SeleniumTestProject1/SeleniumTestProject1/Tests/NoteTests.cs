﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;


namespace SeleniumTestProject1
{
    [TestFixture]
    public class NoteCreationTest : AuthBase
    {
        public static IEnumerable<NoteData> NoteDataFromXml()
        {
            return (List<NoteData>)new XmlSerializer(typeof(List<NoteData>)).Deserialize(new StreamReader(@"Data/notes.xml"));
        } 

        [Test, TestCaseSource("NoteDataFromXml")]
        public void CreateNoteTest(NoteData note)
        {
            manager.Note.Create(note);
            Thread.Sleep(1000);
            Assert.IsTrue(manager.Note.IsCreated(note));
            manager.Note.Delete();
            manager.Authorization.Logout();
        }
    }
}
