﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class Navigation : Functions
    {
        private string baseURL;
        public Navigation(Manager manager, string baseURL) : base(manager)
        {
            this.baseURL = baseURL;
        }

        public void OpenNote(NoteData note)
        {
            Thread.Sleep(1000);
            driver.FindElement(By.LinkText(note.Title)).Click();
        }

        public void OpenLoginPage()
        {
            driver.FindElement(By.CssSelector("span")).Click();
        }

        public void OpenHomePage()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
        }
    }
}
