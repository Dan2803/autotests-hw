﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class Authorization : Functions
    {
        private static AccountData currentUser = new AccountData(Settings.Login, Settings.Password);
        public Authorization(Manager manager) : base(manager)
        {
            //currentUser = null;
        }

        public void Login(AccountData account)
        {
            if (IsLoggedIn())
            {
                //На сайте не указывается имя текущего пользователя.
                if (currentUser != null) {
                    if (account.Username.Equals(currentUser.Username)) {
                        return;
                    }
                }
                Logout();
            }
            driver.FindElement(By.XPath("//div[@id='bs-example-navbar-collapse-1']/div/ul/li[5]/a/span")).Click();
            driver.FindElement(By.Id("loginEmail")).Clear();
            driver.FindElement(By.Id("loginEmail")).SendKeys(account.Username);
            driver.FindElement(By.XPath("(//input[@id='password'])[2]")).Clear();
            driver.FindElement(By.XPath("(//input[@id='password'])[2]")).SendKeys(account.Password);
            driver.FindElement(By.XPath("(//button[@id='submit'])[2]")).Click();
            currentUser = account;
        }

        public void Logout()
        {
            driver.FindElement(By.XPath("//div[@id='bs-example-navbar-collapse-1']/div/ul/li[6]/a/span")).Click();
        }

        public bool IsLoggedIn()
        {
            return IsElementPresent(By.CssSelector("a[href='/logout']"));
        }

        public bool IsLoginError()
        {
            return IsElementPresent(By.CssSelector("p[class='alert alert-danger']"));
        }
    }
}
