﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class NoteFunctions : Functions
    {
        public NoteFunctions(Manager manager) : base(manager)
        {

        }

        public void Delete()
        {
            driver.FindElement(By.LinkText("Delete")).Click();
        }

        public void Create(NoteData note)
        {
            driver.FindElement(By.Id("edit_title")).Clear();
            driver.FindElement(By.Id("edit_title")).SendKeys(note.Title);
            driver.FindElement(By.Id("edit_textarea")).Clear();
            driver.FindElement(By.Id("edit_textarea")).SendKeys(note.Text);
            driver.FindElement(By.CssSelector("div.col-md-6 > #btnSaveNote")).Click();
        }
    }
}
