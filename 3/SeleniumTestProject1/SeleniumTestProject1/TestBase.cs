﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class TestBase
    {
        protected Manager manager;

        [SetUp]
        public void SetupTest()
        {
            manager = new Manager();
        }

        [TearDown]
        public void TeardownTest()
        {
            manager.Stop();
        }
    }
}
