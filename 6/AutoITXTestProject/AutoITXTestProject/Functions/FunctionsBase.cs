﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoItX3Lib;
using NUnit.Framework;

namespace AutoITXTestProject
{
    public class FunctionsBase
    {
        protected Manager manager;
        protected string winTitle;
        protected AutoItX3 autoX;

        public FunctionsBase(Manager manager)
        {
            this.manager = manager;
            winTitle = manager.WinTitle;
            autoX = manager.AutoX;
        }
    }
}
