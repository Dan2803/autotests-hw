﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace AutoITXTestProject
{
    public class ListBoxerFunctions : FunctionsBase
    {
        public ListBoxerFunctions(Manager manager) : base(manager) { }

        public void AddItemToList(ListBoxerItemData item)
        {
            autoX.ControlCommand(winTitle, "", "ThunderRT6ComboBox1", "SelectString", item.GetTypeString());
            autoX.ControlSetText(winTitle, "", "ThunderRT6TextBox1", item.Text);
            autoX.ControlClick(winTitle, "", "ThunderRT6CommandButton2");
        }

        public bool IsAddedItem(ListBoxerItemData item)
        {
            string firstListItemText = autoX.ControlListView(winTitle, "", "ThunderRT6ListBox1", "FindItem", item.Text, "");
            Debug.WriteLine(item.Text + " found: " + firstListItemText);
            return firstListItemText == item.Text;
        }

        public void TurnOnAscendingOrder()
        {
            autoX.ControlClick(winTitle, "", "ThunderRT6OptionButton2");
        }

        public bool CheckSortOrderAscending()
        {
            if (autoX.ControlCommand(winTitle, "", "ThunderRT6OptionButton2", "IsChecked", "") == "1")
                return true;
            return false;
        }

        public void SelectRange(ValueType range)
        {
            string rangeString;
            switch (range) {
                case ValueType.All:
                    rangeString = "All";
                    break;
                case ValueType.AM:
                    rangeString = "a-m";
                    break;
                case ValueType.NZ:
                    rangeString = "n-z";
                    break;
                default:
                    rangeString = "<none>";
                    break;
            }
            autoX.ControlCommand(winTitle, "", "ThunderRT6ComboBox1", "SelectString", rangeString);
        }

        public String GetCurrentRange()
        {
            return autoX.ControlCommand(winTitle, "", "ThunderRT6ComboBox1", "GetCurrentSelection", "");
        }
    }
}
