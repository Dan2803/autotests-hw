﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoITXTestProject
{
    public enum ValueType
    {
        AM  = 0,
        NZ  = 1,
        All = 2
    }
    public class ListBoxerItemData
    {
        public string Text { get; set; }
        public ValueType Type { get; set; }

        public ListBoxerItemData(string text, ValueType type)
        {
            Text = text;
            Type = type;
        }
        public ListBoxerItemData() { }

        public string GetTypeString()
        {
            if (Type == ValueType.AM)
                return "a-m";
            else if (Type == ValueType.NZ)
                return "n-z";
            else if (Type == ValueType.All)
                return "All";
            else
                return "<none>";
        }
    }
}
