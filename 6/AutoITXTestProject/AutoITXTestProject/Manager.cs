﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoItX3Lib;
using NUnit.Framework;

namespace AutoITXTestProject
{
    public class Manager
    {
        public static string winTitle = "ListBoxer - Untitled";
        private AutoItX3 autoX;
        private ListBoxerFunctions listBoxer;


        public Manager()
        {
            autoX = new AutoItX3();
            autoX.Run(@"C:\Program Files (x86)\ListBoxer\ListBoxer.exe");
            autoX.WinWait(winTitle);
            autoX.WinActivate(winTitle);
            autoX.WinWaitActive(winTitle);

            listBoxer = new ListBoxerFunctions(this);
        }

        public void Stop()
        {
            autoX.WinClose(winTitle);
        }

        public AutoItX3 AutoX { get{ return autoX;} set { autoX = value;} }
        public string WinTitle { get { return winTitle; } }
        public ListBoxerFunctions ListBoxer { get { return listBoxer; } }
    }
}
