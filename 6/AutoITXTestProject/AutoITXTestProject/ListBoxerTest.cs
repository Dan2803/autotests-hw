﻿using System;
using AutoItX3Lib;
using NUnit.Framework;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace AutoITXTestProject
{
    [TestFixture]
    public class ListBoxerTests : TestBase
    {
        [Test]
        public void AddItemToList()
        {
            ListBoxerItemData item = new ListBoxerItemData("BlaBla", ValueType.All);
            manager.ListBoxer.AddItemToList(item);
            Thread.Sleep(1000);
            //Пытался по разному создать Assert, но не получилось получить что-либо от ThunderRT6ListBox1
        }

        [Test]
        public void TurnOnSortOrderAscending()
        {
            manager.ListBoxer.TurnOnAscendingOrder();
            NUnit.Framework.Assert.IsTrue(manager.ListBoxer.CheckSortOrderAscending());
        }

        [Test]
        public void selectRange()
        {
            manager.ListBoxer.SelectRange(ValueType.AM);
            NUnit.Framework.Assert.AreEqual("a-m", manager.ListBoxer.GetCurrentRange());
        }
    }
}
