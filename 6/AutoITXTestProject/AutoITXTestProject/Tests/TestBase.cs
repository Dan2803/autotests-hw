﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace AutoITXTestProject
{
    public class TestBase
    {
        public Manager manager;

        [TestFixtureSetUp]
        public void RunApp()
        {
            manager = new Manager();
        }

        [TestFixtureTearDown]
        public void StopApp()
        {
            manager.Stop();
        }
    }
}
