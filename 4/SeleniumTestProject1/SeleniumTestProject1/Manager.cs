﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    public class Manager
    {
        private Navigation nav;
        private Authorization auth;
        private NoteFunctions noteF;

        private IWebDriver driver;
        private string baseURL;
        private StringBuilder verificationErrors;

        private static ThreadLocal<Manager> manager = new ThreadLocal<Manager>();

        private Manager()
        {         
            driver = new ChromeDriver(@"C:\Program Files (x86)\Google");
            baseURL = "https://anotepad.com/";
            verificationErrors = new StringBuilder();

            nav = new Navigation(this, baseURL);
            auth = new Authorization(this);
            noteF = new NoteFunctions(this);
        }

        public static Manager GetInstance()
        {
            if (!manager.IsValueCreated)
            {
                Manager man = new Manager();
                man.Navigation.OpenHomePage();
                manager.Value = man;
            }
            return manager.Value;
        }

        ~Manager()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {

            }
        }

        public void Stop()
        {
            driver.Quit();
        }

        public Navigation Navigation
        {
            get
            {
                return nav;
            }
        }

        public Authorization Authorization
        {
            get
            {
                return auth;
            }
        }

        public NoteFunctions Note
        {
            get
            {
                return noteF;
            }
        }

        public IWebDriver Driver
        {
            get
            {
                return driver;
            }
        }
    }
}
