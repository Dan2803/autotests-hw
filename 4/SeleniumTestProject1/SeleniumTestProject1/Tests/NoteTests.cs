﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTestProject1
{
    [TestFixture]
    public class NoteCreationTest : AuthBase
    {
        [Test]
        public void CreateNoteTest()
        {
            NoteData note = new NoteData("Note YYY", "Blah-blah-blah etc.");
            manager.Note.Create(note);
            Thread.Sleep(1000);
            Assert.IsTrue(manager.Note.IsCreated(note));
            manager.Authorization.Logout();
        }
    }
}
